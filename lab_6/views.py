from django.shortcuts import render
from django.http import HttpResponseRedirect

# Create your views here.
response = {}
def index(request):    
    response['author'] = "Widya Syafira"
    html = 'lab_6/lab_6.html'
    return render(request, html, response)
